<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Source;
use Application\Module;

class IndexController extends AbstractActionController
{
    const PATIENT_NAME = 'Alex';

    private $entityManager;

    private $db;

    private $module_model;

    public function __construct($entityManager, $db)
    {
        $this->entityManager = $entityManager;
        $this->db = $db;
        $this->module_model = new Module();
    }

    public function taskoneAction()
    {
        $cache = $this->module_model->getCacheStorage();
        $sources = $cache->getItem('sources_entity_object_orm');
        if (true) {//if (!$sources) {
            $model = new Source();
            $sources = $model->getExpandItemsByPatientNameORM(self::PATIENT_NAME, $this->entityManager);
            $cache->setItems(['sources_entity_object_orm' => $sources]);
        }

        return new ViewModel([
            'sources' => $sources
        ]);
    }

    public function tasktwoAction()
    {
        $cache = $this->module_model->getCacheStorage();
        $sources = $cache->getItem('sources_entity_object_no_orm');
        if (!$sources) {
            $model = new Source();
            $sources = $model->getExpandItemsByPatientNameNoORM(self::PATIENT_NAME, $this->db);
            $cache->setItems(['sources_entity_object_no_orm' => $sources]);
        }

        return new ViewModel([
            'sources' => $sources
        ]);
    }
}
