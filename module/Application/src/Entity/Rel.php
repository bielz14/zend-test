<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Source;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="tb_rel")
 */
class Rel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="medrec_id")
     */
    protected $medrec_id;

    /**
     * @ORM\Column(name="ndc")
     */
    protected $ndc;

    /**
     * @ORM\ManyToMany(targetEntity="\Application\Entity\Source", mappedBy="rels")
     */
    protected $sources;

    /*
     * @return \Application\Entity\Source
     */
    public function getSources()
    {
        return $this->sources = new ArrayCollection();
    }

    public function getMedrecId()
    {
        return $this->medrec_id;
    }

    public function setMedrecId($medrec_id)
    {
        $this->medrec_id = $medrec_id;
    }

    public function getNdc()
    {
        return $this->ndc;
    }

    public function setNdc($ndc)
    {
        $this->ndc = $ndc;
    }

    /*public function setSource($source)
    {
        $this->source = $source;
        $source->addRels($this);
    }*/
}