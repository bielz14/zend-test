<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Rel;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Cache\StorageFactory;
use Zend\Db\Adapter\Adapter;

/**
 * @ORM\Entity
 * @ORM\Table(name="tb_source")
 */
class Source
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="medrec_id")
     */
    protected $medrec_id;

    /**
     * @ORM\Column(name="icd")
     */
    protected $icd;

    /**
     * @ORM\Column(name="patient_name")
     */
    protected $patient_name;

    /**
     * @ORM\ManyToMany(targetEntity="\Application\Entity\Rel", inversedBy="sources")
     * @ORM\JoinTable(name="tb_rel",
     *      joinColumns={@ORM\JoinColumn(name="medrec_id", referencedColumnName="medrec_id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="medrec_id", referencedColumnName="medrec_id")}
     *      )
     */
    protected $rels;

    protected $expand_items_by_patient_name;

    public function __construct()
    {
        $this->rels = new ArrayCollection();
    }

    public function getRels()
    {
        return $this->rels;
    }

    public function getMedrecId()
    {
        return $this->medrec_id;
    }

    public function setMedrecId($medrec_id)
    {
        $this->medrec_id = $medrec_id;
    }

    public function getIcd()
    {
        return $this->icd;
    }

    public function setIcd($icd)
    {
        $this->icd = $icd;
    }

    public function getPatientName()
    {
        return $this->patient_name;
    }

    public function setPatientName($patient_name)
    {
        $this->patient_name = $patient_name;
    }

    public function getExpandItemsByPatientNameORM($patient_name, $entityManager)
    {
        $this->expand_items_by_patient_name = $entityManager->getRepository(Source::class)->createQueryBuilder('s')
                    ->where('s.patient_name LIKE :patient_name')
                    ->setParameter('patient_name', '%' . $patient_name . '%')
                    ->innerJoin(
                        'Application\Entity\Rel', 'r', \Doctrine\ORM\Query\Expr\Join::WITH, 's.medrec_id = r.medrec_id'
                    )
                    ->getQuery()
                    ->getResult();

        return $this->expand_items_by_patient_name;
    }

    public function getExpandItemsByPatientNameNoORM($patient_name, $db)
    {
        $queryStr = 'SELECT * FROM `tb_source` INNER JOIN `tb_rel` 
                            ON `tb_source`.medrec_id = `tb_rel`.medrec_id 
                            AND `tb_source`.patient_name LIKE "%' . $patient_name . '%"';

        $this->expand_items_by_patient_name = $db->query($queryStr, Adapter::QUERY_MODE_EXECUTE)
            ->toArray();

        return $this->expand_items_by_patient_name;
    }
}